#!/bin/env python3

import json


D = {}

with open('vocab.txt') as fi:
    for line in fi:
        D[line.split()[1]] = line.split()[0]

json.dump(D, open('vocab_dict.json', 'w'))
