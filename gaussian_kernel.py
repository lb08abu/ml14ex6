#!/usr/bin/env python3

import numpy as np


def gaussian_kernel(x1, x2, sig=20):
    """
    Returns a similarity between x1 and x2 column vectors.

    :param x1: x1 vector
    :param x2: x2 vector
    :param sig: simga value
    :return: similarity
    """

    return np.exp(-np.sum((x1 - x2)**2) / (2 * sig**2))

